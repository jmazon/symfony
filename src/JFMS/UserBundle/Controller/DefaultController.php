<?php

namespace JFMS\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('JFMSUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
