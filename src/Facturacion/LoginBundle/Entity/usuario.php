<?php

namespace Facturacion\LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * usuario
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class usuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="correo_usuario", type="string", length=80)
     */
    private $correoUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="password_usuario", type="string", length=80)
     */
    private $passwordUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_usuario", type="string", length=80)
     */
    private $nombreUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil_usuario", type="string", length=80)
     */
    private $perfilUsuario;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set correoUsuario
     *
     * @param string $correoUsuario
     * @return usuario
     */
    public function setCorreoUsuario($correoUsuario)
    {
        $this->correoUsuario = $correoUsuario;

        return $this;
    }

    /**
     * Get correoUsuario
     *
     * @return string 
     */
    public function getCorreoUsuario()
    {
        return $this->correoUsuario;
    }

    /**
     * Set passwordUsuario
     *
     * @param string $passwordUsuario
     * @return usuario
     */
    public function setPasswordUsuario($passwordUsuario)
    {
        $this->passwordUsuario = $passwordUsuario;

        return $this;
    }

    /**
     * Get passwordUsuario
     *
     * @return string 
     */
    public function getPasswordUsuario()
    {
        return $this->passwordUsuario;
    }

    /**
     * Set nombreUsuario
     *
     * @param string $nombreUsuario
     * @return usuario
     */
    public function setNombreUsuario($nombreUsuario)
    {
        $this->nombreUsuario = $nombreUsuario;

        return $this;
    }

    /**
     * Get nombreUsuario
     *
     * @return string 
     */
    public function getNombreUsuario()
    {
        return $this->nombreUsuario;
    }

    /**
     * Set perfilUsuario
     *
     * @param string $perfilUsuario
     * @return usuario
     */
    public function setPerfilUsuario($perfilUsuario)
    {
        $this->perfilUsuario = $perfilUsuario;

        return $this;
    }

    /**
     * Get perfilUsuario
     *
     * @return string 
     */
    public function getPerfilUsuario()
    {
        return $this->perfilUsuario;
    }
}
