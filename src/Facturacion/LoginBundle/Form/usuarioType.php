<?php

namespace Facturacion\LoginBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class usuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('correoUsuario')
            ->add('passwordUsuario')
            ->add('nombreUsuario')
            ->add('perfilUsuario')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Facturacion\LoginBundle\Entity\usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'facturacion_loginbundle_usuario';
    }
}
